﻿using System;
using System.IO;

namespace JPG_Detector
{
    class Program
    {
        private const string Path = "C:\\Users\\Perus\\Documents\\BitBucket\\emedia\\JPG_Detector\\JPG_Detector\\JPG_Image\\image3.jpg"; // mozna uzyc: image1/2/3/4/5
        
        static void Main(string[] args)
        {
            byte[] byteArray = File.ReadAllBytes(Path);

            for (int i = 0; i < (byteArray.Length) ; i++) //  byteArray iteration
            {          
               
                if (byteArray[i] == 0xff && byteArray[i + 1] == 0xd8) // looking for start of image (SOI)
                {
                    Console.WriteLine("Start of Image was detected... ");
                    
                }
                else if (byteArray[i] == 0xff && byteArray[i + 1] == 0xc0 ) // SOF0 marker indefier
                {
                    // SOF0 : Marker indefier (2 bajty) -> Lenght (2 bajty) -> Data precision (1 bajt) ->
                    // -> Image Height (2 bajty) -> Image Width (2 bajty) -> NumberOfComponents (1 bajt) -> Each component

                    int lenght = (byteArray[i + 2])*256 + (byteArray[i + 3]);
                    int dataPrecision = byteArray[i + 4];
                    int imageHeight = (byteArray[i + 5])*256 + (byteArray[i + 6]);
                    int imageWidth = (byteArray[i + 7])*256 + (byteArray[i + 8]);
                    int numberOfComponents = byteArray[i + 9];
                    int eachComponent_componentID = byteArray[i + 10];
                    int eachComponent_samplingFactors = byteArray[i + 11];
                    int eachComponent_quantizationTableNumber = byteArray[i + 12];
                    /*                           EACH COMPONENT
                                                 Read each component data of 3 bytes. It contains,
                                                (component Id(1byte)(1 = Y, 2 = Cb, 3 = Cr, 4 = I, 5 = Q),   
                                                 sampling factors (1byte) (bit 0-3 vertical., 4-7 horizontal.)
                                                 quantization table number (1 byte)
                     */

                    Console.WriteLine("JPG info: ");
                    Console.WriteLine("Lenght: " + lenght);
                    Console.WriteLine("Data precision: " + dataPrecision);
                    Console.WriteLine("Height: " + imageHeight  );
                    Console.WriteLine("Width: " + imageWidth   );
                }
                else if (byteArray[i] == 0xff && byteArray[i + 1] == 0xd9) // looking for end of image (EOI)
                {
                    Console.WriteLine("End of Image was detected... ");
                }
            }
             Console.ReadLine();  
        }
    }
}